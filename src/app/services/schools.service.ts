import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SchoolsService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  httpHeaders = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  constructor(private _http: HttpClient) {

  }
  private apiUrl = '/api/schools';

  getSchools() {
    return this._http.get(this.apiUrl);
  }
  deleteSchool(id) {
    const url = `${this.apiUrl}/${id}`;

    return this._http.delete<any>(url, this.httpOptions);


  }
}

