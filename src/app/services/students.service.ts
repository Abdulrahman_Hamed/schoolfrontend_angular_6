import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  httpHeaders = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  constructor(private _http: HttpClient) {

  }
  private apiUrl = '/api/students';

  getStudents() {
    return this._http.get(this.apiUrl);
  }
  deleteStudent(id) {
    const url = `${this.apiUrl}/${id}`;

    return this._http.delete<any>(url, this.httpOptions);


  }
}
