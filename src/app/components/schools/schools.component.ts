import { Component, OnInit } from '@angular/core';
import { SchoolsService } from '../../services/schools.service';

@Component({
  selector: 'app-schools',
  templateUrl: './schools.component.html',
  styleUrls: ['./schools.component.css']
})
export class SchoolsComponent implements OnInit {

  schools;
  selectedSchool;
  constructor(private _schoolsService: SchoolsService) { }

  ngOnInit() {
    this._schoolsService.getSchools().subscribe((res) => {
      this.schools = res;
    }, (err) => {
      console.log(err);
    });
  }
  onSelect(school) {
    this.selectedSchool = school;
  }
  onDelete(school) {
    console.log(school._id);
    this._schoolsService.deleteSchool(school._id).subscribe();
  }

}
