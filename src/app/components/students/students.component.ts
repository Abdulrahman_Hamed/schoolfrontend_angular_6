import { Component, OnInit } from '@angular/core';
import { StudentsService } from '../../services/students.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  students;
  selectedStudent;
  constructor(private _studentsService: StudentsService) { }

  ngOnInit() {
    this._studentsService.getStudents().subscribe((res) => {
      this.students = res;
    }, (err) => {
      console.log(err);
    });
  }
  onSelect(student) {
    this.selectedStudent = student;
  }
  onDelete(student) {
    console.log(student._id);
    this._studentsService.deleteStudent(student._id).subscribe();
  }

}
